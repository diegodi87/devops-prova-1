t=
up:
	docker compose up || docker-compose up
down:
	docker compose down || docker-compose down
php:
	docker exec -it devops-example-php bash
install:
	composer install
start:
	php public/index.php
test:
	vendor/bin/phpunit --testsuite u --testdox